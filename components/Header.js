import headerStyles from '../styles/Header.module.css'
const Header = props => {
    return (
        <div>
            <h1 className={headerStyles.title}>
                <span>Country</span> Details
            </h1>
            <style jsx>
                {`
                    .title{
                        color: red;
                    }
                `}
            </style>
        </div>
    )
}

export default Header
