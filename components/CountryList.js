import countryStyles from '../styles/CountryList.module.css'
import CountryItems from './CountryItems'
const CountryList = ({countries}) => {
    return (
        <div className={countryStyles.grid}>
            {countries.map((country) => (
            <CountryItems country={country} />
            //<p>{country.name}</p>
            ))}
        </div>
    )
}

export default CountryList
