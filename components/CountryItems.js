import countryStyles from '../styles/CountryList.module.css'
import Link from 'next/link'
const CountryItems = ({country}) => {
    //console.log(country.name);
    return (
        <div>


            

            

                <a className={countryStyles.card}>
                    
                    <img src={`https://www.countryflags.io/${country.alpha2Code}/flat/64.png`} as={`https://www.countryflags.io/${country.alpha2Code}/flat/64.png`}></img>
                    <h3>{country.name}</h3>
                    <p>{country.alpha3Code}</p>
                    <Link href="/detail/[id]" as={`/detail/${country.alpha3Code}`}><button>Show Details</button></Link>
                    <Link href={`https://www.google.com/maps/place/${country.name}`}><button>Show Map</button></Link>
                </a>

        </div>
    )
}

export default CountryItems
