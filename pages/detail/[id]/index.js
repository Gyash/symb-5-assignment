import { useRouter } from 'next/router'
import { useEffect } from 'react'

const detail = ({country}) => {
    console.log(country)
    /*const router = useRouter()
    const {id} = router.query
  
  useEffect(() => {
    if(!id) {
      return;
    }
    const fetchSomethingById = async () => {
      const response = await fetch(`https://restcountries.com/v2/alpha/${id}`)
      const country = await response.json();
      console.log(country);
    }
    fetchSomethingById()
  }, [id])*/
    return <>
            <p>Details of {country.name}</p>
            <img width="300px" height="200px" src={`https://www.countryflags.io/${country.alpha2Code}/flat/64.png`} as={`https://www.countryflags.io/${country.alpha2Code}/flat/64.png`}></img>
            <p>Native Name: {country.nativeName}</p>
            <p>Capital: {country.capital}</p>
            <p>Region: {country.region}</p>
            <p>Sub-region: {country.subregion}</p>
            <p>alpha-3 code: {country.alpha3Code}</p>
            <p>Calling Code: {country.callingCodes}</p>
            <p>Timezone: {country.timezones}</p>
            <p>Population: {country.population}</p>
        </>
}

export const getServerSideProps = async (context) => {
    const res = await fetch(`https://restcountries.com/v2/alpha/${context.params.id}`)
    const country = await res.json();
    return {
        props: {
            country
        }
    }
}



export default detail