import Head from 'next/head'
import CountryList from '../components/CountryList'
export default function Home({countries}) {
  //console.log(countries);
  return (
    <div>
      <Head>
        <title>Countries</title>
        <meta name='keyword' content='web, development, programming'/>

      </Head>
      <CountryList countries={countries} />
      
    </div>
  )
}



export const getStaticProps = async ()=>{
  const res = await fetch('https://restcountries.com/v2/all')
  const countries = await res.json()

  return{
    props: {
      countries
    }
  }
}