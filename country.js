console.log("Ajax");

let fetchBtn = document.getElementById('fetchBtn');
fetchBtn.addEventListener('click',buttonClickHandler)

function buttonClickHandler(){
    console.log('You have clicked the fetchBtn');
    let input = document.getElementById('input_id');
    var my_element = document.getElementById(input.value);

    my_element.scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "nearest"
    });
}
let popBtn = document.getElementById('popBtn');
popBtn.addEventListener('click',popHandler)

function popHandler(){
    console.log('You have clicked the popBtn');
    const xhr = new XMLHttpRequest();

    xhr.open('GET', 'https://pkgstore.datahub.io/core/country-codes/country-codes_json/data/616b1fb83cbfd4eb6d9e7d52924bb00a/country-codes_json.json', true);

    xhr.onprogress = function(){
        console.log('on progress');
    }
    xhr.onload = function(){
        let obj = JSON.parse(this.responseText);
        console.log(obj);
        //let list = document.getElementById('list');
        //str = "";
        for( key in obj){
            if(obj[key].official_name_en==null){continue;}
            //console.log('in for loop');
            //console.log(obj[key]['ISO3166-1-Alpha-3'])
            //str+=`<li>${obj[key].official_name_en}</li>`
            let query = obj[key]['ISO3166-1-Alpha-3'];
            var anc = document.createElement("A");
            anc.setAttribute("href", "E:\SYMB_Internship\Assign 5\symb-5-assignment\details.html?code="+query);
            anc.setAttribute("id", query);
            let element = document.createElement("div");
            var img = document.createElement("img");
            img.src='https://www.countryflags.io/'+obj[key]['ISO3166-1-Alpha-2']+'/flat/64.png';
            var name = document.createElement("h2");
            name.innerHTML= obj[key].official_name_en;
            var code3 = document.createElement("P");
            code3.innerHTML= obj[key]['ISO3166-1-Alpha-3'];
            
            document.getElementById('indiv').appendChild(anc);
            anc.appendChild(element);
            element.appendChild(img);
            element.appendChild(name);
            element.appendChild(code3);
        }
        //list.innerHTML = str;
    }
    xhr.send();
}